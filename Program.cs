﻿using ManyToManyTest.Data;
using ManyToManyTest.Models;

static List<Car> GetAllCars()
{
    using CarDealershipDbContext db = new();
    List<Car> cars = new();

    cars = db.Car.ToList();

    return cars;
}

static void PrintCars(List<Car> carsToPrint)
{
    foreach (Car car in carsToPrint)
        PrintCar(car);
}

static void PrintCar(Car car)
{
    Console.WriteLine($"{car.Id} {car.Name} {car.BrandId}");
}

static void AddCar(Car newCar)
{
    using CarDealershipDbContext db = new();

    db.Car.Add(newCar);

    db.SaveChanges();
}

static void DeleteCar(Car carToDelete)
{
    using CarDealershipDbContext db = new();

    db.Car.Remove(carToDelete);

    db.SaveChanges();
}

static void UpdateCar(int id, Car updatedCar)
{
    using CarDealershipDbContext db = new();

    var carToUpdate = db.Car.Where(car => car.Id == id).Single();
    
    carToUpdate.Name = updatedCar.Name;
    carToUpdate.BrandId = updatedCar.BrandId;

    db.SaveChanges();
}

static Car GetCarById(int id)
{
    using CarDealershipDbContext db = new();

    Car car = db.Car.SingleOrDefault(car => car.Id == id);

    return car;
}

static List<Car> GetCarsByName(string name)
{
    using CarDealershipDbContext db = new();

    List<Car> car = db.Car.Where(car => car.Name == name).ToList();
    
    return car;
}

Console.WriteLine("GET ALL CARS\n");
PrintCars(GetAllCars());

Console.WriteLine("\nGET CAR BY ID (1)\n");
PrintCar(GetCarById(1));

Console.WriteLine("\nGET CAR BY NAME (Polo)\n");
PrintCars(GetCarsByName("Polo"));


Console.WriteLine("\nADD NEW CAR (Beetle)\n");

Car newCar = new() { Name = "Beetle", BrandId = 1 };

AddCar(newCar);
PrintCars(GetAllCars());

Console.WriteLine("\nUPDATE CAR (Beetle -> Kombi)\n");

Car updatedCar = new() { Id = 7, Name = "Kombi", BrandId = 1 };
UpdateCar(updatedCar.Id, updatedCar);

PrintCars(GetAllCars());

DeleteCar(updatedCar);


# Entity Framework Demo
A demonstration of using Entity Framework with a C# console application. Setting up model classes, as well as a DbConfig class. Specifically demonstrating using the code first workflow to design a many-to-many relationship between the Car and Customer entity, with a custom field storing a DateTime object of when a particular customer purchased a particular car. A demo from the Sweden 2023 full stack bootcamp.


## Installation

To clone this repository, run the following command:

```bash
git clone https://gitlab.com/teacher.warren.english/entity-framework-sweden-23.git
```

## Contributing

Warren West (@teacher.warren.english)

## License
©️ Noroff Accelerate
[MIT](https://choosealicense.com/licenses/mit/)
﻿using ManyToManyTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyToManyTest.Data
{
    internal static class SeedHelper
    {
        public static List<Brand> GetBrandSeeds()
        {
            List<Brand> brandSeeds = new()
            {
                new Brand { Id = 1, Name = "VW" },
                new Brand { Id = 2, Name = "BMW" },
                new Brand { Id = 3, Name = "Ford" },
            };

            return brandSeeds;
        }

        public static List<Car> GetCarSeeds()
        {
            List<Car> carSeeds = new()
            {
                new Car() { Id = 1, Name = "Polo", BrandId = 1 },
                new Car() { Id = 2, Name = "Golf", BrandId = 1 },
                new Car() { Id = 3, Name = "X5", BrandId = 2 },
                new Car() { Id = 4, Name = "M3", BrandId = 2 },
                new Car() { Id = 5, Name = "Figo", BrandId = 3 },
                new Car() { Id = 6, Name = "Focus", BrandId = 3 },
            };

            return carSeeds;
        }

        public static List<Customer> GetCustomerSeeds()
        {
            List<Customer> customerSeeds = new()
            { 
                new Customer() { Id = 1, Name = "Warren" },
                new Customer() { Id = 2, Name = "Dean" },
                new Customer() { Id = 3, Name = "Reza" },
            };

            return customerSeeds;
        }

        public static List<CarCustomer> GetCarCustomerSeeds()
        {
            List<CarCustomer> carCustomerSeeds = new()
            {
                // Warren
                new CarCustomer() { CarId = 1, CustomerId = 1, DateOfPurchase = new DateTime(2023, 3, 3) },
                new CarCustomer() { CarId = 5, CustomerId = 1, DateOfPurchase = new DateTime(2023, 6, 10) },
                new CarCustomer() { CarId = 6, CustomerId = 1, DateOfPurchase = new DateTime(2023, 9, 13) },
                // Dean
                new CarCustomer() { CarId = 2, CustomerId = 2, DateOfPurchase = new DateTime(2023, 4, 8) },
                new CarCustomer() { CarId = 3, CustomerId = 2, DateOfPurchase = new DateTime(2023, 7, 16) },
                new CarCustomer() { CarId = 5, CustomerId = 2, DateOfPurchase = new DateTime(2023, 10, 29) },
                // Reza
                new CarCustomer() { CarId = 4, CustomerId = 3, DateOfPurchase = new DateTime(2023, 5, 5) },
                new CarCustomer() { CarId = 5, CustomerId = 3, DateOfPurchase = new DateTime(2023, 8, 15) },
                new CarCustomer() { CarId = 1, CustomerId = 3, DateOfPurchase = new DateTime(2023, 11, 25) },
            };

            return carCustomerSeeds;
        }
    }
}

﻿using ManyToManyTest.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyToManyTest.Data
{
    internal class CarDealershipDbContext : DbContext
    {
        public DbSet<Car> Car { get; set; }
        public DbSet<Customer> Customer{ get; set; }
        public DbSet<Brand> Brand { get; set; }
        public DbSet<CarCustomer> CarCustomer { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            SqlConnectionStringBuilder builder = new()
            {
                DataSource = "localhost\\SQLEXPRESS",
                InitialCatalog = "CarDealership",
                IntegratedSecurity = true,
                TrustServerCertificate = true,
            };

            optionsBuilder.UseSqlServer(builder.ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Describe relationships
            modelBuilder.Entity<CarCustomer>().HasKey(cc => new { cc.CarId, cc.CustomerId });

            // Seed Data
            modelBuilder.Entity<Brand>().HasData(SeedHelper.GetBrandSeeds());
            modelBuilder.Entity<Car>().HasData(SeedHelper.GetCarSeeds());
            modelBuilder.Entity<Customer>().HasData(SeedHelper.GetCustomerSeeds());
            // Seed Relational Data
            modelBuilder.Entity<CarCustomer>().HasData(SeedHelper.GetCarCustomerSeeds());
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ManyToManyTest.Models
{
    internal class Car
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        // FK
        public int BrandId { get; set; }

        // Navigation Props
        public ICollection<CarCustomer> CarCustomers { get; set; }
        public Brand Brand { get; set; }
    }
}

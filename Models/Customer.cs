﻿using System.ComponentModel.DataAnnotations;

namespace ManyToManyTest.Models
{
    internal class Customer
    {
        // PK
        [Key]
        public int Id { get; set; }
        [MaxLength(100)]
        public string Name { get; set; }

        // Navigation Props
        public ICollection<CarCustomer> CarCustomers { get; set; }
    }
}

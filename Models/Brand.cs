﻿using System.ComponentModel.DataAnnotations;

namespace ManyToManyTest.Models
{
    internal class Brand
    {
        // PK
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }

        // Navigation Props
        public ICollection<Car> Cars { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyToManyTest.Models
{
    internal class CarCustomer
    {
        // PK FK Composite Keys
        [Key]
        public int CarId { get; set; }
        [Key]
        public int CustomerId { get; set; }

        public DateTime DateOfPurchase { get; set; }
        // Navigation Props
        public Car Car { get; set; }
        public Customer Customer { get; set; }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace ManyToManyTest.Migrations
{
    /// <inheritdoc />
    public partial class SeedData : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Brand",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "VW" },
                    { 2, "BMW" },
                    { 3, "Ford" }
                });

            migrationBuilder.InsertData(
                table: "Customer",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Warren" },
                    { 2, "Dean" },
                    { 3, "Reza" }
                });

            migrationBuilder.InsertData(
                table: "Car",
                columns: new[] { "Id", "BrandId", "Name" },
                values: new object[,]
                {
                    { 1, 1, "Polo" },
                    { 2, 1, "Golf" },
                    { 3, 2, "X5" },
                    { 4, 2, "M3" },
                    { 5, 3, "Figo" },
                    { 6, 3, "Focus" }
                });

            migrationBuilder.InsertData(
                table: "CarCustomer",
                columns: new[] { "CarId", "CustomerId", "DateOfPurchase" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2023, 3, 3, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 1, 3, new DateTime(2023, 11, 25, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 2, new DateTime(2023, 4, 8, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 2, new DateTime(2023, 7, 16, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 4, 3, new DateTime(2023, 5, 5, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 5, 1, new DateTime(2023, 6, 10, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 5, 2, new DateTime(2023, 10, 29, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 5, 3, new DateTime(2023, 8, 15, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 6, 1, new DateTime(2023, 9, 13, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CarCustomer",
                keyColumns: new[] { "CarId", "CustomerId" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "CarCustomer",
                keyColumns: new[] { "CarId", "CustomerId" },
                keyValues: new object[] { 1, 3 });

            migrationBuilder.DeleteData(
                table: "CarCustomer",
                keyColumns: new[] { "CarId", "CustomerId" },
                keyValues: new object[] { 2, 2 });

            migrationBuilder.DeleteData(
                table: "CarCustomer",
                keyColumns: new[] { "CarId", "CustomerId" },
                keyValues: new object[] { 3, 2 });

            migrationBuilder.DeleteData(
                table: "CarCustomer",
                keyColumns: new[] { "CarId", "CustomerId" },
                keyValues: new object[] { 4, 3 });

            migrationBuilder.DeleteData(
                table: "CarCustomer",
                keyColumns: new[] { "CarId", "CustomerId" },
                keyValues: new object[] { 5, 1 });

            migrationBuilder.DeleteData(
                table: "CarCustomer",
                keyColumns: new[] { "CarId", "CustomerId" },
                keyValues: new object[] { 5, 2 });

            migrationBuilder.DeleteData(
                table: "CarCustomer",
                keyColumns: new[] { "CarId", "CustomerId" },
                keyValues: new object[] { 5, 3 });

            migrationBuilder.DeleteData(
                table: "CarCustomer",
                keyColumns: new[] { "CarId", "CustomerId" },
                keyValues: new object[] { 6, 1 });

            migrationBuilder.DeleteData(
                table: "Car",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Car",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Car",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Car",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Car",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Car",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Customer",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Brand",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Brand",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Brand",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}
